package com.przetocki.wmc;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.przetocki.wmc.domain.DeviceDetails;

public class Home extends AppCompatActivity {

    private Intent gpsStatusIS;

    private LocalDBService dbService;

    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        View mainView = getWindow().getDecorView();
        mainView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
        if(this.getActionBar() !=null){
            this.getActionBar().hide();
        }

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        gpsStatusIS = new Intent(this, GpsStatusIntentService.class);
        this.startService(gpsStatusIS);
        WebView myWebView = (WebView) findViewById(R.id.masterWebView);
        /*Button button = (Button) findViewById(R.id.toggleButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbService.insertDeviceDetailsToDB(new DeviceDetails(1l, 1l, "52", "18"), true);
            }
        });*/
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        myWebView.loadUrl("http://vps433783.ovh.net:8888/");
        dbService = new LocalDBService(this);

        LinearLayout.LayoutParams layoutParams =
            new LinearLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        myWebView.setLayoutParams(layoutParams);
    }

    public void turnGPSOn(View v) {
        dbService.insertDeviceDetailsToDB(new DeviceDetails(1l, 1l, "52", "18"), true);
    }

    @JavascriptInterface
    public void getUserID() {

    }

}
