package com.przetocki.wmc.domain;

import android.content.ContentValues;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;

import java.util.List;
import java.util.UUID;

/**
 * Created by A637017 on 2017-07-10.
 */


public class DeviceDetails {
    @NonNull
    private Long id;
    private Long parentUserID;
    private Long deviceId;
    private String longitude;
    private String latitude;

    @NonNull
    public Long getId() { return id; }

    public void setId(@NonNull Long id) { this.id = id; }

    public Long getDeviceId() { return deviceId; }

    public void setDeviceId(Long deviceId) { this.deviceId = deviceId; }

    public String getLongitude() { return longitude; }

    public void setLongitude(String longitude) { this.longitude = longitude; }

    public String getLatitude() { return latitude; }

    public void setLatitude(String latitude) { this.latitude = latitude; }

    public Long getParentUserID() { return parentUserID; }

    public void setParentUserID(Long parentUserID) { this.parentUserID = parentUserID; }

    public DeviceDetails(@NonNull Long id, Long parentUserID, Long deviceId, String longitude, String latitude) {
        this.id = id;
        this.parentUserID = parentUserID;
        this.deviceId = deviceId;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public DeviceDetails(Long parentUserID, Long deviceId, String longitude, String latitude) {
        this.parentUserID = parentUserID;
        this.deviceId = deviceId;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public ContentValues getContentValuesToInsert(boolean createNew){
        ContentValues values = new ContentValues();
//        values.put("ID", getId());
        values.put("PARENTUSERID", getParentUserID());
        values.put("DEVICEID", (String) (createNew ? UUID.randomUUID().toString() : getDeviceId()));
        values.put("LONGITUDE", getLongitude());
        values.put("LATITUDE", getLatitude());
        return values;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DeviceDetails)) return false;

        DeviceDetails that = (DeviceDetails) o;

        if (!getId().equals(that.getId())) return false;
        if (getParentUserID() != null ? !getParentUserID().equals(that.getParentUserID()) : that.getParentUserID() != null)
            return false;
        if (getDeviceId() != null ? !getDeviceId().equals(that.getDeviceId()) : that.getDeviceId() != null)
            return false;
        if (getLongitude() != null ? !getLongitude().equals(that.getLongitude()) : that.getLongitude() != null)
            return false;
        return getLatitude() != null ? getLatitude().equals(that.getLatitude()) : that.getLatitude() == null;

    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + (getParentUserID() != null ? getParentUserID().hashCode() : 0);
        result = 31 * result + (getDeviceId() != null ? getDeviceId().hashCode() : 0);
        result = 31 * result + (getLongitude() != null ? getLongitude().hashCode() : 0);
        result = 31 * result + (getLatitude() != null ? getLatitude().hashCode() : 0);
        return result;
    }
}
