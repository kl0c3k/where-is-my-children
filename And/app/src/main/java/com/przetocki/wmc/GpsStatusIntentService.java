package com.przetocki.wmc;

import android.Manifest;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

/**
 * Created by A637017 on 2017-07-06.
 */

public class GpsStatusIntentService extends IntentService {
    LocationManager locationManager;

    public static Location location;

    public GpsStatusIntentService(String name) {
        super(name);

    }

    public GpsStatusIntentService(){
        this(GpsStatusIntentService.class.getName());

    }


    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        locationManager = (LocationManager)
            getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListener = new MyLocationListener();
//        locationManager.getAllProviders();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            startActivity(new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION));
        }
        locationManager.requestLocationUpdates(
            LocationManager.GPS_PROVIDER, 5000, 10, locationListener);
        Utils.showToast("Starting GpsStatusIntentService", getBaseContext());
        while (1==1){

        }
    }


}
