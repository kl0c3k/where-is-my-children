package com.przetocki.wmc;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

/**
 * Created by A637017 on 2017-07-06.
 */

public class Utils extends Activity {

    public static void showToast(final String msg, final Context context) {
        //gets the main thread
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                // run this code in the main thread
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
