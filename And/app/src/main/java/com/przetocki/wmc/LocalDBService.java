package com.przetocki.wmc;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.przetocki.wmc.domain.DeviceDetails;

/**
 * Created by A637017 on 2017-07-10.
 */

public class LocalDBService extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "wmc.db";
    private static final String CREATE_DATABASE_QUERY =
        "CREATE TABLE DeviceDetails (ID INTEGER PRIMARY KEY," +
            " PARENTUSERID TEXT, DEVICEID TEXT, LONGITUDE TEXT, LATITUDE TEXT);";
    private static final String DROP_DATABASE_QUERY = "DROP TABLE IF EXISTS DEVICEDETAILS";
    private SQLiteDatabase db;


    public LocalDBService(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
        db = getWritableDatabase();
    }

    public LocalDBService(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        db = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_DATABASE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(DROP_DATABASE_QUERY);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }


    public int insertDeviceDetailsToDB(DeviceDetails dd, boolean createNew) {
        return (int) db.insert("DEVICEDETAILS", null, dd.getContentValuesToInsert(createNew));
    }
}
