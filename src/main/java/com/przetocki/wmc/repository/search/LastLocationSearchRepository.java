package com.przetocki.wmc.repository.search;

import com.przetocki.wmc.domain.LastLocation;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the LastLocation entity.
 */
public interface LastLocationSearchRepository extends ElasticsearchRepository<LastLocation, Long> {
}
