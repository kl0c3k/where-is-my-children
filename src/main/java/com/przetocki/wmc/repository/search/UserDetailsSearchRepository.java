package com.przetocki.wmc.repository.search;

import com.przetocki.wmc.domain.UserDetails;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the UserDetails entity.
 */
public interface UserDetailsSearchRepository extends ElasticsearchRepository<UserDetails, Long> {
}
