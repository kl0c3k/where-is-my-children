package com.przetocki.wmc.repository.search;

import com.przetocki.wmc.domain.ParentUser;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ParentUser entity.
 */
public interface ParentUserSearchRepository extends ElasticsearchRepository<ParentUser, Long> {
}
