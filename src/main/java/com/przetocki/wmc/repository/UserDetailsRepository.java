package com.przetocki.wmc.repository;

import com.przetocki.wmc.domain.UserDetails;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the UserDetails entity.
 */
@SuppressWarnings("unused")
public interface UserDetailsRepository extends JpaRepository<UserDetails,Long> {

    @Query("select distinct userDetails from UserDetails userDetails left join fetch userDetails.childrens left join fetch userDetails.parents")
    List<UserDetails> findAllWithEagerRelationships();

    @Query("select userDetails from UserDetails userDetails left join fetch userDetails.childrens left join fetch userDetails.parents where userDetails.id =:id")
    UserDetails findOneWithEagerRelationships(@Param("id") Long id);

}
