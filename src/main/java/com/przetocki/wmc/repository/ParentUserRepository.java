package com.przetocki.wmc.repository;

import com.przetocki.wmc.domain.ParentUser;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ParentUser entity.
 */
@SuppressWarnings("unused")
public interface ParentUserRepository extends JpaRepository<ParentUser,Long> {

}
