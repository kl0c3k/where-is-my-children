package com.przetocki.wmc.repository;

import com.przetocki.wmc.domain.LastLocation;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the LastLocation entity.
 */
@SuppressWarnings("unused")
public interface LastLocationRepository extends JpaRepository<LastLocation,Long> {

}
