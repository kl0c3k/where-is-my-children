/**
 * View Models used by Spring MVC REST controllers.
 */
package com.przetocki.wmc.web.rest.vm;
