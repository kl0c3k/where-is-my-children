package com.przetocki.wmc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.przetocki.wmc.service.LastLocationService;
import com.przetocki.wmc.web.rest.util.HeaderUtil;
import com.przetocki.wmc.web.rest.util.PaginationUtil;
import com.przetocki.wmc.service.dto.LastLocationDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing LastLocation.
 */
@RestController
@RequestMapping("/api")
public class LastLocationResource {

    private final Logger log = LoggerFactory.getLogger(LastLocationResource.class);

    private static final String ENTITY_NAME = "lastLocation";
        
    private final LastLocationService lastLocationService;

    public LastLocationResource(LastLocationService lastLocationService) {
        this.lastLocationService = lastLocationService;
    }

    /**
     * POST  /last-locations : Create a new lastLocation.
     *
     * @param lastLocationDTO the lastLocationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new lastLocationDTO, or with status 400 (Bad Request) if the lastLocation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/last-locations")
    @Timed
    public ResponseEntity<LastLocationDTO> createLastLocation(@Valid @RequestBody LastLocationDTO lastLocationDTO) throws URISyntaxException {
        log.debug("REST request to save LastLocation : {}", lastLocationDTO);
        if (lastLocationDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new lastLocation cannot already have an ID")).body(null);
        }
        LastLocationDTO result = lastLocationService.save(lastLocationDTO);
        return ResponseEntity.created(new URI("/api/last-locations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /last-locations : Updates an existing lastLocation.
     *
     * @param lastLocationDTO the lastLocationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated lastLocationDTO,
     * or with status 400 (Bad Request) if the lastLocationDTO is not valid,
     * or with status 500 (Internal Server Error) if the lastLocationDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/last-locations")
    @Timed
    public ResponseEntity<LastLocationDTO> updateLastLocation(@Valid @RequestBody LastLocationDTO lastLocationDTO) throws URISyntaxException {
        log.debug("REST request to update LastLocation : {}", lastLocationDTO);
        if (lastLocationDTO.getId() == null) {
            return createLastLocation(lastLocationDTO);
        }
        LastLocationDTO result = lastLocationService.save(lastLocationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, lastLocationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /last-locations : get all the lastLocations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of lastLocations in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/last-locations")
    @Timed
    public ResponseEntity<List<LastLocationDTO>> getAllLastLocations(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of LastLocations");
        Page<LastLocationDTO> page = lastLocationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/last-locations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /last-locations/:id : get the "id" lastLocation.
     *
     * @param id the id of the lastLocationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the lastLocationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/last-locations/{id}")
    @Timed
    public ResponseEntity<LastLocationDTO> getLastLocation(@PathVariable Long id) {
        log.debug("REST request to get LastLocation : {}", id);
        LastLocationDTO lastLocationDTO = lastLocationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(lastLocationDTO));
    }

    /**
     * DELETE  /last-locations/:id : delete the "id" lastLocation.
     *
     * @param id the id of the lastLocationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/last-locations/{id}")
    @Timed
    public ResponseEntity<Void> deleteLastLocation(@PathVariable Long id) {
        log.debug("REST request to delete LastLocation : {}", id);
        lastLocationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/last-locations?query=:query : search for the lastLocation corresponding
     * to the query.
     *
     * @param query the query of the lastLocation search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/last-locations")
    @Timed
    public ResponseEntity<List<LastLocationDTO>> searchLastLocations(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of LastLocations for query {}", query);
        Page<LastLocationDTO> page = lastLocationService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/last-locations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
