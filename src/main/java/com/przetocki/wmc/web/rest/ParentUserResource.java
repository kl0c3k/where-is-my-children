package com.przetocki.wmc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.przetocki.wmc.service.ParentUserService;
import com.przetocki.wmc.web.rest.util.HeaderUtil;
import com.przetocki.wmc.web.rest.util.PaginationUtil;
import com.przetocki.wmc.service.dto.ParentUserDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ParentUser.
 */
@RestController
@RequestMapping("/api")
public class ParentUserResource {

    private final Logger log = LoggerFactory.getLogger(ParentUserResource.class);

    private static final String ENTITY_NAME = "parentUser";
        
    private final ParentUserService parentUserService;

    public ParentUserResource(ParentUserService parentUserService) {
        this.parentUserService = parentUserService;
    }

    /**
     * POST  /parent-users : Create a new parentUser.
     *
     * @param parentUserDTO the parentUserDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new parentUserDTO, or with status 400 (Bad Request) if the parentUser has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/parent-users")
    @Timed
    public ResponseEntity<ParentUserDTO> createParentUser(@Valid @RequestBody ParentUserDTO parentUserDTO) throws URISyntaxException {
        log.debug("REST request to save ParentUser : {}", parentUserDTO);
        if (parentUserDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new parentUser cannot already have an ID")).body(null);
        }
        ParentUserDTO result = parentUserService.save(parentUserDTO);
        return ResponseEntity.created(new URI("/api/parent-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /parent-users : Updates an existing parentUser.
     *
     * @param parentUserDTO the parentUserDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated parentUserDTO,
     * or with status 400 (Bad Request) if the parentUserDTO is not valid,
     * or with status 500 (Internal Server Error) if the parentUserDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/parent-users")
    @Timed
    public ResponseEntity<ParentUserDTO> updateParentUser(@Valid @RequestBody ParentUserDTO parentUserDTO) throws URISyntaxException {
        log.debug("REST request to update ParentUser : {}", parentUserDTO);
        if (parentUserDTO.getId() == null) {
            return createParentUser(parentUserDTO);
        }
        ParentUserDTO result = parentUserService.save(parentUserDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, parentUserDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /parent-users : get all the parentUsers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of parentUsers in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/parent-users")
    @Timed
    public ResponseEntity<List<ParentUserDTO>> getAllParentUsers(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ParentUsers");
        Page<ParentUserDTO> page = parentUserService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/parent-users");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /parent-users/:id : get the "id" parentUser.
     *
     * @param id the id of the parentUserDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the parentUserDTO, or with status 404 (Not Found)
     */
    @GetMapping("/parent-users/{id}")
    @Timed
    public ResponseEntity<ParentUserDTO> getParentUser(@PathVariable Long id) {
        log.debug("REST request to get ParentUser : {}", id);
        ParentUserDTO parentUserDTO = parentUserService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(parentUserDTO));
    }

    /**
     * DELETE  /parent-users/:id : delete the "id" parentUser.
     *
     * @param id the id of the parentUserDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/parent-users/{id}")
    @Timed
    public ResponseEntity<Void> deleteParentUser(@PathVariable Long id) {
        log.debug("REST request to delete ParentUser : {}", id);
        parentUserService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/parent-users?query=:query : search for the parentUser corresponding
     * to the query.
     *
     * @param query the query of the parentUser search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/parent-users")
    @Timed
    public ResponseEntity<List<ParentUserDTO>> searchParentUsers(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of ParentUsers for query {}", query);
        Page<ParentUserDTO> page = parentUserService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/parent-users");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
