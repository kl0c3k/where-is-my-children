package com.przetocki.wmc.service.mapper;

import com.przetocki.wmc.domain.*;
import com.przetocki.wmc.service.dto.UserDetailsDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity UserDetails and its DTO UserDetailsDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface UserDetailsMapper {

    @Mapping(source = "userId.id", target = "userIdId")
    @Mapping(source = "userId.login", target = "userIdLogin")
    @Mapping(source = "device.id", target = "deviceId")
    UserDetailsDTO userDetailsToUserDetailsDTO(UserDetails userDetails);

    List<UserDetailsDTO> userDetailsToUserDetailsDTOs(List<UserDetails> userDetails);

    @Mapping(source = "userIdId", target = "userId")
    @Mapping(source = "deviceId", target = "device")
    UserDetails userDetailsDTOToUserDetails(UserDetailsDTO userDetailsDTO);

    List<UserDetails> userDetailsDTOsToUserDetails(List<UserDetailsDTO> userDetailsDTOs);

    default UserDetails userDetailsFromId(Long id) {
        if (id == null) {
            return null;
        }
        UserDetails userDetails = new UserDetails();
        userDetails.setId(id);
        return userDetails;
    }

    default Device deviceFromId(Long id) {
        if (id == null) {
            return null;
        }
        Device device = new Device();
        device.setId(id);
        return device;
    }
}
