package com.przetocki.wmc.service.mapper;

import com.przetocki.wmc.domain.*;
import com.przetocki.wmc.service.dto.LastLocationDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity LastLocation and its DTO LastLocationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LastLocationMapper {

    @Mapping(source = "device.id", target = "deviceId")
    LastLocationDTO lastLocationToLastLocationDTO(LastLocation lastLocation);

    List<LastLocationDTO> lastLocationsToLastLocationDTOs(List<LastLocation> lastLocations);

    @Mapping(source = "deviceId", target = "device")
    LastLocation lastLocationDTOToLastLocation(LastLocationDTO lastLocationDTO);

    List<LastLocation> lastLocationDTOsToLastLocations(List<LastLocationDTO> lastLocationDTOs);

    default Device deviceFromId(Long id) {
        if (id == null) {
            return null;
        }
        Device device = new Device();
        device.setId(id);
        return device;
    }
}
