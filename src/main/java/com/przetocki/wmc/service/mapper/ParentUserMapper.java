package com.przetocki.wmc.service.mapper;

import com.przetocki.wmc.domain.*;
import com.przetocki.wmc.service.dto.ParentUserDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity ParentUser and its DTO ParentUserDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, })
public interface ParentUserMapper {

    @Mapping(source = "userIds.id", target = "userIdsId")
    @Mapping(source = "userIds.login", target = "userIdsLogin")
    ParentUserDTO parentUserToParentUserDTO(ParentUser parentUser);

    List<ParentUserDTO> parentUsersToParentUserDTOs(List<ParentUser> parentUsers);

    @Mapping(source = "userIdsId", target = "userIds")
    ParentUser parentUserDTOToParentUser(ParentUserDTO parentUserDTO);

    List<ParentUser> parentUserDTOsToParentUsers(List<ParentUserDTO> parentUserDTOs);
}
