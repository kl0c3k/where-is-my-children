package com.przetocki.wmc.service.impl;

import com.przetocki.wmc.service.UserDetailsService;
import com.przetocki.wmc.domain.UserDetails;
import com.przetocki.wmc.repository.UserDetailsRepository;
import com.przetocki.wmc.repository.search.UserDetailsSearchRepository;
import com.przetocki.wmc.service.dto.UserDetailsDTO;
import com.przetocki.wmc.service.mapper.UserDetailsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing UserDetails.
 */
@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService{

    private final Logger log = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    private final UserDetailsRepository userDetailsRepository;

    private final UserDetailsMapper userDetailsMapper;

    private final UserDetailsSearchRepository userDetailsSearchRepository;

    public UserDetailsServiceImpl(UserDetailsRepository userDetailsRepository, UserDetailsMapper userDetailsMapper, UserDetailsSearchRepository userDetailsSearchRepository) {
        this.userDetailsRepository = userDetailsRepository;
        this.userDetailsMapper = userDetailsMapper;
        this.userDetailsSearchRepository = userDetailsSearchRepository;
    }

    /**
     * Save a userDetails.
     *
     * @param userDetailsDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public UserDetailsDTO save(UserDetailsDTO userDetailsDTO) {
        log.debug("Request to save UserDetails : {}", userDetailsDTO);
        UserDetails userDetails = userDetailsMapper.userDetailsDTOToUserDetails(userDetailsDTO);
        userDetails = userDetailsRepository.save(userDetails);
        UserDetailsDTO result = userDetailsMapper.userDetailsToUserDetailsDTO(userDetails);
        userDetailsSearchRepository.save(userDetails);
        return result;
    }

    /**
     *  Get all the userDetails.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserDetailsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserDetails");
        Page<UserDetails> result = userDetailsRepository.findAll(pageable);
        return result.map(userDetails -> userDetailsMapper.userDetailsToUserDetailsDTO(userDetails));
    }



    /**
     *  Get one userDetails by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public UserDetailsDTO findOne(Long id) {
        log.debug("Request to get UserDetails : {}", id);
        UserDetails userDetails = userDetailsRepository.findOneWithEagerRelationships(id);
        UserDetailsDTO userDetailsDTO = userDetailsMapper.userDetailsToUserDetailsDTO(userDetails);
        return userDetailsDTO;
    }

    /**
     *  Delete the  userDetails by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserDetails : {}", id);
        userDetailsRepository.delete(id);
        userDetailsSearchRepository.delete(id);
    }

    /**
     * Search for the userDetails corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserDetailsDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of UserDetails for query {}", query);
        Page<UserDetails> result = userDetailsSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(userDetails -> userDetailsMapper.userDetailsToUserDetailsDTO(userDetails));
    }
}
