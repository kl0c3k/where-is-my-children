package com.przetocki.wmc.service.impl;

import com.przetocki.wmc.service.LastLocationService;
import com.przetocki.wmc.domain.LastLocation;
import com.przetocki.wmc.repository.LastLocationRepository;
import com.przetocki.wmc.repository.search.LastLocationSearchRepository;
import com.przetocki.wmc.service.dto.LastLocationDTO;
import com.przetocki.wmc.service.mapper.LastLocationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing LastLocation.
 */
@Service
@Transactional
public class LastLocationServiceImpl implements LastLocationService{

    private final Logger log = LoggerFactory.getLogger(LastLocationServiceImpl.class);
    
    private final LastLocationRepository lastLocationRepository;

    private final LastLocationMapper lastLocationMapper;

    private final LastLocationSearchRepository lastLocationSearchRepository;

    public LastLocationServiceImpl(LastLocationRepository lastLocationRepository, LastLocationMapper lastLocationMapper, LastLocationSearchRepository lastLocationSearchRepository) {
        this.lastLocationRepository = lastLocationRepository;
        this.lastLocationMapper = lastLocationMapper;
        this.lastLocationSearchRepository = lastLocationSearchRepository;
    }

    /**
     * Save a lastLocation.
     *
     * @param lastLocationDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public LastLocationDTO save(LastLocationDTO lastLocationDTO) {
        log.debug("Request to save LastLocation : {}", lastLocationDTO);
        LastLocation lastLocation = lastLocationMapper.lastLocationDTOToLastLocation(lastLocationDTO);
        lastLocation = lastLocationRepository.save(lastLocation);
        LastLocationDTO result = lastLocationMapper.lastLocationToLastLocationDTO(lastLocation);
        lastLocationSearchRepository.save(lastLocation);
        return result;
    }

    /**
     *  Get all the lastLocations.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<LastLocationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all LastLocations");
        Page<LastLocation> result = lastLocationRepository.findAll(pageable);
        return result.map(lastLocation -> lastLocationMapper.lastLocationToLastLocationDTO(lastLocation));
    }

    /**
     *  Get one lastLocation by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public LastLocationDTO findOne(Long id) {
        log.debug("Request to get LastLocation : {}", id);
        LastLocation lastLocation = lastLocationRepository.findOne(id);
        LastLocationDTO lastLocationDTO = lastLocationMapper.lastLocationToLastLocationDTO(lastLocation);
        return lastLocationDTO;
    }

    /**
     *  Delete the  lastLocation by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete LastLocation : {}", id);
        lastLocationRepository.delete(id);
        lastLocationSearchRepository.delete(id);
    }

    /**
     * Search for the lastLocation corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<LastLocationDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of LastLocations for query {}", query);
        Page<LastLocation> result = lastLocationSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(lastLocation -> lastLocationMapper.lastLocationToLastLocationDTO(lastLocation));
    }
}
