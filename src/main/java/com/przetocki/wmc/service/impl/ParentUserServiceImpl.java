package com.przetocki.wmc.service.impl;

import com.przetocki.wmc.service.ParentUserService;
import com.przetocki.wmc.domain.ParentUser;
import com.przetocki.wmc.repository.ParentUserRepository;
import com.przetocki.wmc.repository.search.ParentUserSearchRepository;
import com.przetocki.wmc.service.dto.ParentUserDTO;
import com.przetocki.wmc.service.mapper.ParentUserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ParentUser.
 */
@Service
@Transactional
public class ParentUserServiceImpl implements ParentUserService{

    private final Logger log = LoggerFactory.getLogger(ParentUserServiceImpl.class);
    
    private final ParentUserRepository parentUserRepository;

    private final ParentUserMapper parentUserMapper;

    private final ParentUserSearchRepository parentUserSearchRepository;

    public ParentUserServiceImpl(ParentUserRepository parentUserRepository, ParentUserMapper parentUserMapper, ParentUserSearchRepository parentUserSearchRepository) {
        this.parentUserRepository = parentUserRepository;
        this.parentUserMapper = parentUserMapper;
        this.parentUserSearchRepository = parentUserSearchRepository;
    }

    /**
     * Save a parentUser.
     *
     * @param parentUserDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ParentUserDTO save(ParentUserDTO parentUserDTO) {
        log.debug("Request to save ParentUser : {}", parentUserDTO);
        ParentUser parentUser = parentUserMapper.parentUserDTOToParentUser(parentUserDTO);
        parentUser = parentUserRepository.save(parentUser);
        ParentUserDTO result = parentUserMapper.parentUserToParentUserDTO(parentUser);
        parentUserSearchRepository.save(parentUser);
        return result;
    }

    /**
     *  Get all the parentUsers.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ParentUserDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ParentUsers");
        Page<ParentUser> result = parentUserRepository.findAll(pageable);
        return result.map(parentUser -> parentUserMapper.parentUserToParentUserDTO(parentUser));
    }

    /**
     *  Get one parentUser by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ParentUserDTO findOne(Long id) {
        log.debug("Request to get ParentUser : {}", id);
        ParentUser parentUser = parentUserRepository.findOne(id);
        ParentUserDTO parentUserDTO = parentUserMapper.parentUserToParentUserDTO(parentUser);
        return parentUserDTO;
    }

    /**
     *  Delete the  parentUser by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ParentUser : {}", id);
        parentUserRepository.delete(id);
        parentUserSearchRepository.delete(id);
    }

    /**
     * Search for the parentUser corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ParentUserDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ParentUsers for query {}", query);
        Page<ParentUser> result = parentUserSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(parentUser -> parentUserMapper.parentUserToParentUserDTO(parentUser));
    }
}
