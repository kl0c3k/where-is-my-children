package com.przetocki.wmc.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the ParentUser entity.
 */
public class ParentUserDTO implements Serializable {

    private Long id;

    @NotNull
    private Long userId;

    private Long userIdsId;

    private String userIdsLogin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserIdsId() {
        return userIdsId;
    }

    public void setUserIdsId(Long userId) {
        this.userIdsId = userId;
    }

    public String getUserIdsLogin() {
        return userIdsLogin;
    }

    public void setUserIdsLogin(String userLogin) {
        this.userIdsLogin = userLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ParentUserDTO parentUserDTO = (ParentUserDTO) o;

        if ( ! Objects.equals(id, parentUserDTO.id)) { return false; }

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ParentUserDTO{" +
            "id=" + id +
            ", userId='" + userId + "'" +
            '}';
    }
}
