package com.przetocki.wmc.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the UserDetails entity.
 */
public class UserDetailsDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 3)
    private String nick;

    private String name;

    private String surname;

    @NotNull
    private String phone;

    @NotNull
    private Boolean isParent;

    private Integer age;

    @NotNull
    private Boolean acceptedTerms;

    private String lastKnownLocation;

    private String photoServerUrl;

    private Long userIdId;

    private String userIdLogin;

    private Set<UserDetailsDTO> childrens = new HashSet<>();

    private Set<UserDetailsDTO> parents = new HashSet<>();

    private Long deviceId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    public Boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(Boolean isParent) {
        this.isParent = isParent;
    }
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
    public Boolean getAcceptedTerms() {
        return acceptedTerms;
    }

    public void setAcceptedTerms(Boolean acceptedTerms) {
        this.acceptedTerms = acceptedTerms;
    }
    public String getLastKnownLocation() {
        return lastKnownLocation;
    }

    public void setLastKnownLocation(String lastKnownLocation) {
        this.lastKnownLocation = lastKnownLocation;
    }
    public String getPhotoServerUrl() {
        return photoServerUrl;
    }

    public void setPhotoServerUrl(String photoServerUrl) {
        this.photoServerUrl = photoServerUrl;
    }

    public Long getUserIdId() {
        return userIdId;
    }

    public void setUserIdId(Long userId) {
        this.userIdId = userId;
    }

    public String getUserIdLogin() {
        return userIdLogin;
    }

    public void setUserIdLogin(String userLogin) {
        this.userIdLogin = userLogin;
    }

    public Set<UserDetailsDTO> getChildrens() {
        return childrens;
    }

    public void setChildrens(Set<UserDetailsDTO> userDetails) {
        this.childrens = userDetails;
    }

    public Set<UserDetailsDTO> getParents() {
        return parents;
    }

    public void setParents(Set<UserDetailsDTO> userDetails) {
        this.parents = userDetails;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserDetailsDTO userDetailsDTO = (UserDetailsDTO) o;

        if ( ! Objects.equals(id, userDetailsDTO.id)) { return false; }

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "UserDetailsDTO{" +
            "id=" + id +
            ", nick='" + nick + "'" +
            ", name='" + name + "'" +
            ", surname='" + surname + "'" +
            ", phone='" + phone + "'" +
            ", isParent='" + isParent + "'" +
            ", age='" + age + "'" +
            ", acceptedTerms='" + acceptedTerms + "'" +
            ", lastKnownLocation='" + lastKnownLocation + "'" +
            ", photoServerUrl='" + photoServerUrl + "'" +
            '}';
    }
}
