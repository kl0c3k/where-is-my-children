package com.przetocki.wmc.service;

import com.przetocki.wmc.service.dto.UserDetailsDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * Service Interface for managing UserDetails.
 */
public interface UserDetailsService {

    /**
     * Save a userDetails.
     *
     * @param userDetailsDTO the entity to save
     * @return the persisted entity
     */
    UserDetailsDTO save(UserDetailsDTO userDetailsDTO);

    /**
     *  Get all the userDetails.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<UserDetailsDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" userDetails.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    UserDetailsDTO findOne(Long id);

    /**
     *  Delete the "id" userDetails.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the userDetails corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<UserDetailsDTO> search(String query, Pageable pageable);
}
