package com.przetocki.wmc.service;

import com.przetocki.wmc.service.dto.ParentUserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * Service Interface for managing ParentUser.
 */
public interface ParentUserService {

    /**
     * Save a parentUser.
     *
     * @param parentUserDTO the entity to save
     * @return the persisted entity
     */
    ParentUserDTO save(ParentUserDTO parentUserDTO);

    /**
     *  Get all the parentUsers.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ParentUserDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" parentUser.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ParentUserDTO findOne(Long id);

    /**
     *  Delete the "id" parentUser.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the parentUser corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ParentUserDTO> search(String query, Pageable pageable);
}
