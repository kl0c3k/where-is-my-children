package com.przetocki.wmc.service;

import com.przetocki.wmc.service.dto.LastLocationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * Service Interface for managing LastLocation.
 */
public interface LastLocationService {

    /**
     * Save a lastLocation.
     *
     * @param lastLocationDTO the entity to save
     * @return the persisted entity
     */
    LastLocationDTO save(LastLocationDTO lastLocationDTO);

    /**
     *  Get all the lastLocations.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<LastLocationDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" lastLocation.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    LastLocationDTO findOne(Long id);

    /**
     *  Delete the "id" lastLocation.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the lastLocation corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<LastLocationDTO> search(String query, Pageable pageable);
}
