package com.przetocki.wmc.domain.enumeration;

/**
 * The DeviceType enumeration.
 */
public enum DeviceType {
    COMPUTER,PHONE,TABLET,OTHER
}
