package com.przetocki.wmc.domain;

import org.hibernate.annotations.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CascadeType;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A UserDetails.
 */
@Entity
@Table(name = "user_details")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "userdetails")
public class UserDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 3)
    @Column(name = "nick", nullable = false)
    private String nick;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @NotNull
    @Column(name = "phone", nullable = false)
    private String phone;

    @NotNull
    @Column(name = "is_parent", nullable = false)
    private Boolean isParent;

    @Column(name = "age")
    private Integer age;

    @NotNull
    @Column(name = "accepted_terms", nullable = false)
    private Boolean acceptedTerms;

    @Column(name = "last_known_location")
    private String lastKnownLocation;

    @Column(name = "photo_server_url")
    private String photoServerUrl;

    @OneToOne
    @JoinColumn(unique = true)
    private User userId;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "user_details_childrens",
               joinColumns = @JoinColumn(name="user_details_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="childrens_id", referencedColumnName="id"))
    private Set<UserDetails> childrens = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "user_details_parents",
               joinColumns = @JoinColumn(name="user_details_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="parents_id", referencedColumnName="id"))
    private Set<UserDetails> parents = new HashSet<>();

    @ManyToOne
    @Cascade(CascadeType.ALL)
    private Device device;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public UserDetails nick(String nick) {
        this.nick = nick;
        return this;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getName() {
        return name;
    }

    public UserDetails name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public UserDetails surname(String surname) {
        this.surname = surname;
        return this;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public UserDetails phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean isIsParent() {
        return isParent;
    }

    public UserDetails isParent(Boolean isParent) {
        this.isParent = isParent;
        return this;
    }

    public void setIsParent(Boolean isParent) {
        this.isParent = isParent;
    }

    public Integer getAge() {
        return age;
    }

    public UserDetails age(Integer age) {
        this.age = age;
        return this;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean isAcceptedTerms() {
        return acceptedTerms;
    }

    public UserDetails acceptedTerms(Boolean acceptedTerms) {
        this.acceptedTerms = acceptedTerms;
        return this;
    }

    public void setAcceptedTerms(Boolean acceptedTerms) {
        this.acceptedTerms = acceptedTerms;
    }

    public String getLastKnownLocation() {
        return lastKnownLocation;
    }

    public UserDetails lastKnownLocation(String lastKnownLocation) {
        this.lastKnownLocation = lastKnownLocation;
        return this;
    }

    public void setLastKnownLocation(String lastKnownLocation) {
        this.lastKnownLocation = lastKnownLocation;
    }

    public String getPhotoServerUrl() {
        return photoServerUrl;
    }

    public UserDetails photoServerUrl(String photoServerUrl) {
        this.photoServerUrl = photoServerUrl;
        return this;
    }

    public void setPhotoServerUrl(String photoServerUrl) {
        this.photoServerUrl = photoServerUrl;
    }

    public User getUserId() {
        return userId;
    }

    public UserDetails userId(User user) {
        this.userId = user;
        return this;
    }

    public void setUserId(User user) {
        this.userId = user;
    }

    public Set<UserDetails> getChildrens() {
        return childrens;
    }

    public UserDetails childrens(Set<UserDetails> userDetails) {
        this.childrens = userDetails;
        return this;
    }

    public UserDetails addChildrens(UserDetails userDetails) {
        this.childrens.add(userDetails);
        userDetails.getChildrens().add(this);
        return this;
    }

    public UserDetails removeChildrens(UserDetails userDetails) {
        this.childrens.remove(userDetails);
        userDetails.getChildrens().remove(this);
        return this;
    }

    public void setChildrens(Set<UserDetails> userDetails) {
        this.childrens = userDetails;
    }

    public Set<UserDetails> getParents() {
        return parents;
    }

    public UserDetails parents(Set<UserDetails> userDetails) {
        this.parents = userDetails;
        return this;
    }

    public UserDetails addParents(UserDetails userDetails) {
        this.parents.add(userDetails);
        userDetails.getParents().add(this);
        return this;
    }

    public UserDetails removeParents(UserDetails userDetails) {
        this.parents.remove(userDetails);
        userDetails.getParents().remove(this);
        return this;
    }

    public void setParents(Set<UserDetails> userDetails) {
        this.parents = userDetails;
    }

    public Device getDevice() {
        return device;
    }

    public UserDetails device(Device device) {
        this.device = device;
        return this;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserDetails userDetails = (UserDetails) o;
        if (userDetails.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, userDetails.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "UserDetails{" +
            "id=" + id +
            ", nick='" + nick + "'" +
            ", name='" + name + "'" +
            ", surname='" + surname + "'" +
            ", phone='" + phone + "'" +
            ", isParent='" + isParent + "'" +
            ", age='" + age + "'" +
            ", acceptedTerms='" + acceptedTerms + "'" +
            ", lastKnownLocation='" + lastKnownLocation + "'" +
            ", photoServerUrl='" + photoServerUrl + "'" +
            '}';
    }
}
