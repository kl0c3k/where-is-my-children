(function() {
    'use strict';

    angular
        .module('wmcApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
