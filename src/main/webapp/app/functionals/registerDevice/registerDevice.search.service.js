(function() {
    'use strict';

    angular
        .module('wmcApp')
        .factory('RegisteredDevicesSearch', RegisteredDevicesSearch);

    RegisteredDevicesSearch.$inject = ['$resource'];

    function RegisteredDevicesSearch($resource) {
        var resourceUrl =  'api/_search/devices/';


        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: false},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    return data;
                }
            },
            'put': {
                method: 'PUT'
            }

        });
    }
})();
