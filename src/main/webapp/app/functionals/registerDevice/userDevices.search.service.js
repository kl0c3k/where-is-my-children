(function() {
    'use strict';

    angular
        .module('wmcApp')
        .factory('UserDevices', UserDevices);

    UserDevices.$inject = ['$resource'];

    function UserDevices($resource) {
        var resourceUrl =  'api/userdevices';


        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: false},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    return data;
                }
            },
            'put': {
                method: 'PUT'
            }

        });
    }
})();
