(function() {
    'use strict';

    angular
        .module('wmcApp')
        .controller('RegisterDevice', RegisterDevice);

    RegisterDevice.$inject = ['$scope', '$state', 'Principal', 'Device', 'UserDetails', 'UserDevices'];

    function RegisterDevice ($scope, $state, Principal, Device, UserDetails, UserDevices) {
        var vm = this;



        //enter view
        vm.device = null;
        vm.userDetails = null;
        vm.account = null;
        vm.isAuthenticated = null;
        vm.userId = null;
        vm.save = save;
        vm.userDevices = [];


        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        getAccount();
//        vm.userDevices = getUserDevices();

//        getUserId();arr

        //not logged == redirect v
        //logged =>
        //check devices count
        //if < 0
        //add new device
            //else
            //show devices dashboard.

























        function save () {

            vm.isSaving = true;
            vm.isSaving = true;
            if(vm.userDetails && vm.device){
              saveOrUpdateDevice();

            }
        }

        function saveOrUpdateUserDetails(){
            if (vm.userDetails.id !== null) {
                UserDetails.update(vm.userDetails, onSaveSuccessUD, onSaveError);
            } else {
                UserDetails.save(vm.userDetails, onSaveSuccessUD, onSaveError);
            }
        }

        function saveOrUpdateDevice(){
              if (vm.device.id !== null) {
                    Device.update(vm.device, onSaveSuccessDev, onSaveError);
                } else {
                    Device.save(vm.device, onSaveSuccessDev, onSaveError);
                }
        }
        //w
//        if(userId){
           //t
//            var userKey =  firebase.database().ref().child("users").push().key;
//            var deviceKey =  firebase.database.ref().child("users/device" + userKey).push().key;
//        } else {
            //t

            //Device.
//        }

        function addDeviceToUser(){
//            vm.device =
//            Device.save(vm.device, onSaveSuccess, onSaveError);
        }



        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
//                vm.key =
            });
        }

         function onSaveSuccessDev (result) {
         vm.userDetails.device = {};
            vm.userDetails.device.id = result.id;

             $scope.$emit('wmcApp:deviceUpdate', result);
//             $uibModalInstance.close(result);
             vm.isSaving = false;
             saveOrUpdateUserDetails();
          }
          function onSaveSuccessUD (result) {
               $scope.$emit('wmcApp:userDetailsUpdate', result);
//               $uibModalInstance.close(result);
               vm.isSaving = false;
            }

        function onSaveError () {
            vm.isSaving = false;
        }

        function getUserDevices(){
            UserDevices.get(function (result) {
                vm.userId = result[0];
            });
        }


    }
})();
