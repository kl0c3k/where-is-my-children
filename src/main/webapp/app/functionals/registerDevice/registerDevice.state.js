(function() {
    'use strict';

    angular
        .module('wmcApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('registerDevice', {
            parent: 'app',
            url: '/registerDevice',
            data: {
                authorities: [],
                pageTitle: 'registerDevice'

            },
            views: {
                'content@': {
                    templateUrl: 'app/functionals/registerDevice/registerDevice.html',
                    controller: 'RegisterDevice',
                    controllerAs: 'vm'
                }
            },
            re1solve: {
                mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('registerDevice');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
