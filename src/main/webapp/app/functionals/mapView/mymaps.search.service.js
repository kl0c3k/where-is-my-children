(function() {
    'use strict';

    angular
        .module('wmcApp')
        .factory('MyMapsSearch', MyMapsSearch);

    MyMapsSearch.$inject = ['$resource'];

    function MyMapsSearch($resource) {
        var resourceUrl =  'api/_search/devices/userId';


        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: false},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    return data;
                }
            }
        });
    }
})();
