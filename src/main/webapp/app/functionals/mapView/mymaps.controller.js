(function() {
    'use strict';

    angular
        .module('wmcApp')
        .controller('MyMapsController', MyMapsController);

    MyMapsController.$inject = ['$scope', '$state', 'MyMapsSearch'];

    function MyMapsController ($scope, $state, MyMapsSearch) {
        var vm = this;
        var map = MapMaker();
        vm.userId = 0;
        map.createOSMap(21.0,52.0,10);

        getApi();

        // You can retrieve services via the firebase variable...
        var defaultStorage = firebase.storage();
        var defaultDatabase = firebase.database();

        // ... or you can use the equivalent shorthand notation
        defaultStorage = firebase.storage();
        defaultDatabase = firebase.database();


        for(var i =0; i < 1000; i++){
            map.addMarker(i, Math.random() * (23 - 15.5) + 15.5, Math.random() * (54 - 49.6) + 49.6)
        }

        function getApi(){
            MyMapsSearch.get(function (result) {
                vm.userId = result[0];
            });
        }

        writeUserData(vm.userId);

        function writeUserData(userId) {
          firebase.database().ref('users/' + userId).set({
            userId: vm.userId,
//            email: email,
//            profile_picture : imageUrl
          });
        }
    }
})();
