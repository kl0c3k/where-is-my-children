(function() {
    'use strict';

    angular
        .module('wmcApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('mymaps', {
            parent: 'app',
            url: '/mymaps',
            data: {
                authorities: [],
                pageTitle: 'Maps'

            },
            views: {
                'content@': {
                    templateUrl: 'app/functionals/mapView/mymaps.html',
                    controller: 'MyMapsController',
                    controllerAs: 'vm'
                }
            },
            re1solve: {
                mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('mymaps');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
