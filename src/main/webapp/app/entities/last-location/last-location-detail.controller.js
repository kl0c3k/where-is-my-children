(function() {
    'use strict';

    angular
        .module('wmcApp')
        .controller('LastLocationDetailController', LastLocationDetailController);

    LastLocationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'LastLocation', 'Device'];

    function LastLocationDetailController($scope, $rootScope, $stateParams, previousState, entity, LastLocation, Device) {
        var vm = this;

        vm.lastLocation = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('wmcApp:lastLocationUpdate', function(event, result) {
            vm.lastLocation = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
