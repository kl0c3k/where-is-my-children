(function() {
    'use strict';

    angular
        .module('wmcApp')
        .factory('LastLocationSearch', LastLocationSearch);

    LastLocationSearch.$inject = ['$resource'];

    function LastLocationSearch($resource) {
        var resourceUrl =  'api/_search/last-locations/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
