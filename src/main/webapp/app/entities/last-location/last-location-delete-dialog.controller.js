(function() {
    'use strict';

    angular
        .module('wmcApp')
        .controller('LastLocationDeleteController',LastLocationDeleteController);

    LastLocationDeleteController.$inject = ['$uibModalInstance', 'entity', 'LastLocation'];

    function LastLocationDeleteController($uibModalInstance, entity, LastLocation) {
        var vm = this;

        vm.lastLocation = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            LastLocation.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
