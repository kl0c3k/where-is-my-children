(function() {
    'use strict';
    angular
        .module('wmcApp')
        .factory('LastLocation', LastLocation);

    LastLocation.$inject = ['$resource'];

    function LastLocation ($resource) {
        var resourceUrl =  'api/last-locations/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
