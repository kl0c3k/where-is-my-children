(function() {
    'use strict';

    angular
        .module('wmcApp')
        .controller('LastLocationDialogController', LastLocationDialogController);

    LastLocationDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'LastLocation', 'Device'];

    function LastLocationDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, LastLocation, Device) {
        var vm = this;

        vm.lastLocation = entity;
        vm.clear = clear;
        vm.save = save;
        vm.devices = Device.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.lastLocation.id !== null) {
                LastLocation.update(vm.lastLocation, onSaveSuccess, onSaveError);
            } else {
                LastLocation.save(vm.lastLocation, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('wmcApp:lastLocationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
