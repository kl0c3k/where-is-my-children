(function() {
    'use strict';

    angular
        .module('wmcApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('last-location', {
            parent: 'entity',
            url: '/last-location?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'wmcApp.lastLocation.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/last-location/last-locations.html',
                    controller: 'LastLocationController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('lastLocation');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('last-location-detail', {
            parent: 'last-location',
            url: '/last-location/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'wmcApp.lastLocation.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/last-location/last-location-detail.html',
                    controller: 'LastLocationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('lastLocation');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'LastLocation', function($stateParams, LastLocation) {
                    return LastLocation.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'last-location',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('last-location-detail.edit', {
            parent: 'last-location-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/last-location/last-location-dialog.html',
                    controller: 'LastLocationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['LastLocation', function(LastLocation) {
                            return LastLocation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('last-location.new', {
            parent: 'last-location',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/last-location/last-location-dialog.html',
                    controller: 'LastLocationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                latitude: null,
                                longitude: null,
                                city: null,
                                country: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('last-location', null, { reload: 'last-location' });
                }, function() {
                    $state.go('last-location');
                });
            }]
        })
        .state('last-location.edit', {
            parent: 'last-location',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/last-location/last-location-dialog.html',
                    controller: 'LastLocationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['LastLocation', function(LastLocation) {
                            return LastLocation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('last-location', null, { reload: 'last-location' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('last-location.delete', {
            parent: 'last-location',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/last-location/last-location-delete-dialog.html',
                    controller: 'LastLocationDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['LastLocation', function(LastLocation) {
                            return LastLocation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('last-location', null, { reload: 'last-location' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
