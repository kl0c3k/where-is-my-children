(function() {
    'use strict';
    angular
        .module('wmcApp')
        .factory('UserDetails', UserDetails);

    UserDetails.$inject = ['$resource'];

    function UserDetails ($resource) {
        var resourceUrl =  'api/user-details/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
