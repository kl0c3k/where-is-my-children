(function() {
    'use strict';

    angular
        .module('wmcApp')
        .controller('UserDetailsDialogController', UserDetailsDialogController);

    UserDetailsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'UserDetails', 'User', 'Device'];

    function UserDetailsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, UserDetails, User, Device) {
        var vm = this;

        vm.userDetails = entity;
        vm.clear = clear;
        vm.save = save;
        vm.users = User.query();
        vm.userdetails = UserDetails.query();
        vm.devices = Device.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.userDetails.id !== null) {
                UserDetails.update(vm.userDetails, onSaveSuccess, onSaveError);
            } else {
                UserDetails.save(vm.userDetails, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('wmcApp:userDetailsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
