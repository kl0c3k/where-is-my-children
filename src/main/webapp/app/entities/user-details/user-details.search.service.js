(function() {
    'use strict';

    angular
        .module('wmcApp')
        .factory('UserDetailsSearch', UserDetailsSearch);

    UserDetailsSearch.$inject = ['$resource'];

    function UserDetailsSearch($resource) {
        var resourceUrl =  'api/_search/user-details/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
