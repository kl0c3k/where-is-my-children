(function() {
    'use strict';

    angular
        .module('wmcApp')
        .controller('UserDetailsDeleteController',UserDetailsDeleteController);

    UserDetailsDeleteController.$inject = ['$uibModalInstance', 'entity', 'UserDetails'];

    function UserDetailsDeleteController($uibModalInstance, entity, UserDetails) {
        var vm = this;

        vm.userDetails = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            UserDetails.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
