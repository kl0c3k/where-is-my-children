(function() {
    'use strict';

    angular
        .module('wmcApp')
        .controller('UserDetailsDetailController', UserDetailsDetailController);

    UserDetailsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UserDetails', 'User', 'Device'];

    function UserDetailsDetailController($scope, $rootScope, $stateParams, previousState, entity, UserDetails, User, Device) {
        var vm = this;

        vm.userDetails = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('wmcApp:userDetailsUpdate', function(event, result) {
            vm.userDetails = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
