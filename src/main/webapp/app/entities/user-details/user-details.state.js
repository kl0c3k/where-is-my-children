(function() {
    'use strict';

    angular
        .module('wmcApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('user-details', {
            parent: 'entity',
            url: '/user-details?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'wmcApp.userDetails.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-details/user-details.html',
                    controller: 'UserDetailsController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userDetails');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('user-details-detail', {
            parent: 'user-details',
            url: '/user-details/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'wmcApp.userDetails.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-details/user-details-detail.html',
                    controller: 'UserDetailsDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userDetails');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'UserDetails', function($stateParams, UserDetails) {
                    return UserDetails.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'user-details',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('user-details-detail.edit', {
            parent: 'user-details-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-details/user-details-dialog.html',
                    controller: 'UserDetailsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserDetails', function(UserDetails) {
                            return UserDetails.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-details.new', {
            parent: 'user-details',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-details/user-details-dialog.html',
                    controller: 'UserDetailsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nick: null,
                                name: null,
                                surname: null,
                                phone: null,
                                isParent: false,
                                age: null,
                                acceptedTerms: false,
                                lastKnownLocation: null,
                                photoServerUrl: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('user-details', null, { reload: 'user-details' });
                }, function() {
                    $state.go('user-details');
                });
            }]
        })
        .state('user-details.edit', {
            parent: 'user-details',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-details/user-details-dialog.html',
                    controller: 'UserDetailsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserDetails', function(UserDetails) {
                            return UserDetails.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-details', null, { reload: 'user-details' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-details.delete', {
            parent: 'user-details',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-details/user-details-delete-dialog.html',
                    controller: 'UserDetailsDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['UserDetails', function(UserDetails) {
                            return UserDetails.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-details', null, { reload: 'user-details' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
