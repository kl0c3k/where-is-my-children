(function() {
    'use strict';

    angular
        .module('wmcApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('parent-user', {
            parent: 'entity',
            url: '/parent-user?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'wmcApp.parentUser.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/parent-user/parent-users.html',
                    controller: 'ParentUserController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('parentUser');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('parent-user-detail', {
            parent: 'parent-user',
            url: '/parent-user/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'wmcApp.parentUser.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/parent-user/parent-user-detail.html',
                    controller: 'ParentUserDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('parentUser');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ParentUser', function($stateParams, ParentUser) {
                    return ParentUser.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'parent-user',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('parent-user-detail.edit', {
            parent: 'parent-user-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/parent-user/parent-user-dialog.html',
                    controller: 'ParentUserDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ParentUser', function(ParentUser) {
                            return ParentUser.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('parent-user.new', {
            parent: 'parent-user',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/parent-user/parent-user-dialog.html',
                    controller: 'ParentUserDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                userId: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('parent-user', null, { reload: 'parent-user' });
                }, function() {
                    $state.go('parent-user');
                });
            }]
        })
        .state('parent-user.edit', {
            parent: 'parent-user',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/parent-user/parent-user-dialog.html',
                    controller: 'ParentUserDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ParentUser', function(ParentUser) {
                            return ParentUser.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('parent-user', null, { reload: 'parent-user' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('parent-user.delete', {
            parent: 'parent-user',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/parent-user/parent-user-delete-dialog.html',
                    controller: 'ParentUserDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ParentUser', function(ParentUser) {
                            return ParentUser.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('parent-user', null, { reload: 'parent-user' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
