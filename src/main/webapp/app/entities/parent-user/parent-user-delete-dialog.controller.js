(function() {
    'use strict';

    angular
        .module('wmcApp')
        .controller('ParentUserDeleteController',ParentUserDeleteController);

    ParentUserDeleteController.$inject = ['$uibModalInstance', 'entity', 'ParentUser'];

    function ParentUserDeleteController($uibModalInstance, entity, ParentUser) {
        var vm = this;

        vm.parentUser = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ParentUser.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
