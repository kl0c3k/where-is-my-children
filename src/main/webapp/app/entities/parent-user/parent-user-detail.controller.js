(function() {
    'use strict';

    angular
        .module('wmcApp')
        .controller('ParentUserDetailController', ParentUserDetailController);

    ParentUserDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ParentUser', 'User'];

    function ParentUserDetailController($scope, $rootScope, $stateParams, previousState, entity, ParentUser, User) {
        var vm = this;

        vm.parentUser = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('wmcApp:parentUserUpdate', function(event, result) {
            vm.parentUser = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
