(function() {
    'use strict';

    angular
        .module('wmcApp')
        .factory('ParentUserSearch', ParentUserSearch);

    ParentUserSearch.$inject = ['$resource'];

    function ParentUserSearch($resource) {
        var resourceUrl =  'api/_search/parent-users/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
