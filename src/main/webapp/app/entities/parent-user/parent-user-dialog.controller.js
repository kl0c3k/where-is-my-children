(function() {
    'use strict';

    angular
        .module('wmcApp')
        .controller('ParentUserDialogController', ParentUserDialogController);

    ParentUserDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'ParentUser', 'User'];

    function ParentUserDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, ParentUser, User) {
        var vm = this;

        vm.parentUser = entity;
        vm.clear = clear;
        vm.save = save;
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.parentUser.id !== null) {
                ParentUser.update(vm.parentUser, onSaveSuccess, onSaveError);
            } else {
                ParentUser.save(vm.parentUser, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('wmcApp:parentUserUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
