(function() {
    'use strict';
    angular
        .module('wmcApp')
        .factory('ParentUser', ParentUser);

    ParentUser.$inject = ['$resource'];

    function ParentUser ($resource) {
        var resourceUrl =  'api/parent-users/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
