(function() {
    'use strict';

    angular
        .module('wmcApp')
        .factory('DeviceSearch', DeviceSearch);

    DeviceSearch.$inject = ['$resource'];

    function DeviceSearch($resource) {
        var resourceUrl =  'api/_search/devices/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
