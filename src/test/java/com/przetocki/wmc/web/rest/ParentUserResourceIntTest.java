package com.przetocki.wmc.web.rest;

import com.przetocki.wmc.WmcApp;

import com.przetocki.wmc.domain.ParentUser;
import com.przetocki.wmc.domain.User;
import com.przetocki.wmc.repository.ParentUserRepository;
import com.przetocki.wmc.service.ParentUserService;
import com.przetocki.wmc.repository.search.ParentUserSearchRepository;
import com.przetocki.wmc.service.dto.ParentUserDTO;
import com.przetocki.wmc.service.mapper.ParentUserMapper;
import com.przetocki.wmc.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ParentUserResource REST controller.
 *
 * @see ParentUserResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WmcApp.class)
public class ParentUserResourceIntTest {

    private static final Long DEFAULT_USER_ID = 1L;
    private static final Long UPDATED_USER_ID = 2L;

    @Autowired
    private ParentUserRepository parentUserRepository;

    @Autowired
    private ParentUserMapper parentUserMapper;

    @Autowired
    private ParentUserService parentUserService;

    @Autowired
    private ParentUserSearchRepository parentUserSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restParentUserMockMvc;

    private ParentUser parentUser;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ParentUserResource parentUserResource = new ParentUserResource(parentUserService);
        this.restParentUserMockMvc = MockMvcBuilders.standaloneSetup(parentUserResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ParentUser createEntity(EntityManager em) {
        ParentUser parentUser = new ParentUser();
        // Add required entity
        User userIds = UserResourceIntTest.createEntity(em);
        em.persist(userIds);
        em.flush();
        parentUser.setUserIds(userIds);
        return parentUser;
    }

    @Before
    public void initTest() {
        parentUserSearchRepository.deleteAll();
        parentUser = createEntity(em);
    }

    @Test
    @Transactional
    public void createParentUser() throws Exception {
        int databaseSizeBeforeCreate = parentUserRepository.findAll().size();

        // Create the ParentUser
        ParentUserDTO parentUserDTO = parentUserMapper.parentUserToParentUserDTO(parentUser);
        restParentUserMockMvc.perform(post("/api/parent-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parentUserDTO)))
            .andExpect(status().isCreated());

        // Validate the ParentUser in the database
        List<ParentUser> parentUserList = parentUserRepository.findAll();
        assertThat(parentUserList).hasSize(databaseSizeBeforeCreate + 1);
        ParentUser testParentUser = parentUserList.get(parentUserList.size() - 1);
//        assertThat(testParentUser.getUserId()).isEqualTo(DEFAULT_USER_ID);

        // Validate the ParentUser in Elasticsearch
        ParentUser parentUserEs = parentUserSearchRepository.findOne(testParentUser.getId());
        assertThat(parentUserEs).isEqualToComparingFieldByField(testParentUser);
    }

    @Test
    @Transactional
    public void createParentUserWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = parentUserRepository.findAll().size();

        // Create the ParentUser with an existing ID
        parentUser.setId(1L);
        ParentUserDTO parentUserDTO = parentUserMapper.parentUserToParentUserDTO(parentUser);

        // An entity with an existing ID cannot be created, so this API call must fail
        restParentUserMockMvc.perform(post("/api/parent-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parentUserDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<ParentUser> parentUserList = parentUserRepository.findAll();
        assertThat(parentUserList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = parentUserRepository.findAll().size();
        // set the field null
//        parentUser.setUserId(null);

        // Create the ParentUser, which fails.
        ParentUserDTO parentUserDTO = parentUserMapper.parentUserToParentUserDTO(parentUser);

        restParentUserMockMvc.perform(post("/api/parent-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parentUserDTO)))
            .andExpect(status().isBadRequest());

        List<ParentUser> parentUserList = parentUserRepository.findAll();
        assertThat(parentUserList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllParentUsers() throws Exception {
        // Initialize the database
        parentUserRepository.saveAndFlush(parentUser);

        // Get all the parentUserList
        restParentUserMockMvc.perform(get("/api/parent-users?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(parentUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.intValue())));
    }

    @Test
    @Transactional
    public void getParentUser() throws Exception {
        // Initialize the database
        parentUserRepository.saveAndFlush(parentUser);

        // Get the parentUser
        restParentUserMockMvc.perform(get("/api/parent-users/{id}", parentUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(parentUser.getId().intValue()))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingParentUser() throws Exception {
        // Get the parentUser
        restParentUserMockMvc.perform(get("/api/parent-users/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateParentUser() throws Exception {
        // Initialize the database
        parentUserRepository.saveAndFlush(parentUser);
        parentUserSearchRepository.save(parentUser);
        int databaseSizeBeforeUpdate = parentUserRepository.findAll().size();

        // Update the parentUser
        ParentUser updatedParentUser = parentUserRepository.findOne(parentUser.getId());
//        updatedParentUser
//            .userId(UPDATED_USER_ID);
        ParentUserDTO parentUserDTO = parentUserMapper.parentUserToParentUserDTO(updatedParentUser);

        restParentUserMockMvc.perform(put("/api/parent-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parentUserDTO)))
            .andExpect(status().isOk());

        // Validate the ParentUser in the database
        List<ParentUser> parentUserList = parentUserRepository.findAll();
        assertThat(parentUserList).hasSize(databaseSizeBeforeUpdate);
        ParentUser testParentUser = parentUserList.get(parentUserList.size() - 1);
//        assertThat(testParentUser.getUserId()).isEqualTo(UPDATED_USER_ID);

        // Validate the ParentUser in Elasticsearch
        ParentUser parentUserEs = parentUserSearchRepository.findOne(testParentUser.getId());
        assertThat(parentUserEs).isEqualToComparingFieldByField(testParentUser);
    }

    @Test
    @Transactional
    public void updateNonExistingParentUser() throws Exception {
        int databaseSizeBeforeUpdate = parentUserRepository.findAll().size();

        // Create the ParentUser
        ParentUserDTO parentUserDTO = parentUserMapper.parentUserToParentUserDTO(parentUser);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restParentUserMockMvc.perform(put("/api/parent-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parentUserDTO)))
            .andExpect(status().isCreated());

        // Validate the ParentUser in the database
        List<ParentUser> parentUserList = parentUserRepository.findAll();
        assertThat(parentUserList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteParentUser() throws Exception {
        // Initialize the database
        parentUserRepository.saveAndFlush(parentUser);
        parentUserSearchRepository.save(parentUser);
        int databaseSizeBeforeDelete = parentUserRepository.findAll().size();

        // Get the parentUser
        restParentUserMockMvc.perform(delete("/api/parent-users/{id}", parentUser.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean parentUserExistsInEs = parentUserSearchRepository.exists(parentUser.getId());
        assertThat(parentUserExistsInEs).isFalse();

        // Validate the database is empty
        List<ParentUser> parentUserList = parentUserRepository.findAll();
        assertThat(parentUserList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchParentUser() throws Exception {
        // Initialize the database
        parentUserRepository.saveAndFlush(parentUser);
        parentUserSearchRepository.save(parentUser);

        // Search the parentUser
        restParentUserMockMvc.perform(get("/api/_search/parent-users?query=id:" + parentUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(parentUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ParentUser.class);
    }
}
