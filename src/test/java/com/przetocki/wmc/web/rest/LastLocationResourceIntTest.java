package com.przetocki.wmc.web.rest;

import com.przetocki.wmc.WmcApp;

import com.przetocki.wmc.domain.LastLocation;
import com.przetocki.wmc.domain.Device;
import com.przetocki.wmc.repository.LastLocationRepository;
import com.przetocki.wmc.service.LastLocationService;
import com.przetocki.wmc.repository.search.LastLocationSearchRepository;
import com.przetocki.wmc.service.dto.LastLocationDTO;
import com.przetocki.wmc.service.mapper.LastLocationMapper;
import com.przetocki.wmc.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LastLocationResource REST controller.
 *
 * @see LastLocationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WmcApp.class)
public class LastLocationResourceIntTest {

    private static final String DEFAULT_LATITUDE = "AAAAAAAAAA";
    private static final String UPDATED_LATITUDE = "BBBBBBBBBB";

    private static final String DEFAULT_LONGITUDE = "AAAAAAAAAA";
    private static final String UPDATED_LONGITUDE = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    @Autowired
    private LastLocationRepository lastLocationRepository;

    @Autowired
    private LastLocationMapper lastLocationMapper;

    @Autowired
    private LastLocationService lastLocationService;

    @Autowired
    private LastLocationSearchRepository lastLocationSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restLastLocationMockMvc;

    private LastLocation lastLocation;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        LastLocationResource lastLocationResource = new LastLocationResource(lastLocationService);
        this.restLastLocationMockMvc = MockMvcBuilders.standaloneSetup(lastLocationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LastLocation createEntity(EntityManager em) {
        LastLocation lastLocation = new LastLocation()
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .city(DEFAULT_CITY)
            .country(DEFAULT_COUNTRY);
        // Add required entity
        Device device = DeviceResourceIntTest.createEntity(em);
        em.persist(device);
        em.flush();
        lastLocation.setDevice(device);
        return lastLocation;
    }

    @Before
    public void initTest() {
        lastLocationSearchRepository.deleteAll();
        lastLocation = createEntity(em);
    }

    @Test
    @Transactional
    public void createLastLocation() throws Exception {
        int databaseSizeBeforeCreate = lastLocationRepository.findAll().size();

        // Create the LastLocation
        LastLocationDTO lastLocationDTO = lastLocationMapper.lastLocationToLastLocationDTO(lastLocation);
        restLastLocationMockMvc.perform(post("/api/last-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lastLocationDTO)))
            .andExpect(status().isCreated());

        // Validate the LastLocation in the database
        List<LastLocation> lastLocationList = lastLocationRepository.findAll();
        assertThat(lastLocationList).hasSize(databaseSizeBeforeCreate + 1);
        LastLocation testLastLocation = lastLocationList.get(lastLocationList.size() - 1);
        assertThat(testLastLocation.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testLastLocation.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testLastLocation.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testLastLocation.getCountry()).isEqualTo(DEFAULT_COUNTRY);

        // Validate the LastLocation in Elasticsearch
        LastLocation lastLocationEs = lastLocationSearchRepository.findOne(testLastLocation.getId());
        assertThat(lastLocationEs).isEqualToComparingFieldByField(testLastLocation);
    }

    @Test
    @Transactional
    public void createLastLocationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = lastLocationRepository.findAll().size();

        // Create the LastLocation with an existing ID
        lastLocation.setId(1L);
        LastLocationDTO lastLocationDTO = lastLocationMapper.lastLocationToLastLocationDTO(lastLocation);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLastLocationMockMvc.perform(post("/api/last-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lastLocationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<LastLocation> lastLocationList = lastLocationRepository.findAll();
        assertThat(lastLocationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkLatitudeIsRequired() throws Exception {
        int databaseSizeBeforeTest = lastLocationRepository.findAll().size();
        // set the field null
        lastLocation.setLatitude(null);

        // Create the LastLocation, which fails.
        LastLocationDTO lastLocationDTO = lastLocationMapper.lastLocationToLastLocationDTO(lastLocation);

        restLastLocationMockMvc.perform(post("/api/last-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lastLocationDTO)))
            .andExpect(status().isBadRequest());

        List<LastLocation> lastLocationList = lastLocationRepository.findAll();
        assertThat(lastLocationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLongitudeIsRequired() throws Exception {
        int databaseSizeBeforeTest = lastLocationRepository.findAll().size();
        // set the field null
        lastLocation.setLongitude(null);

        // Create the LastLocation, which fails.
        LastLocationDTO lastLocationDTO = lastLocationMapper.lastLocationToLastLocationDTO(lastLocation);

        restLastLocationMockMvc.perform(post("/api/last-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lastLocationDTO)))
            .andExpect(status().isBadRequest());

        List<LastLocation> lastLocationList = lastLocationRepository.findAll();
        assertThat(lastLocationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCityIsRequired() throws Exception {
        int databaseSizeBeforeTest = lastLocationRepository.findAll().size();
        // set the field null
        lastLocation.setCity(null);

        // Create the LastLocation, which fails.
        LastLocationDTO lastLocationDTO = lastLocationMapper.lastLocationToLastLocationDTO(lastLocation);

        restLastLocationMockMvc.perform(post("/api/last-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lastLocationDTO)))
            .andExpect(status().isBadRequest());

        List<LastLocation> lastLocationList = lastLocationRepository.findAll();
        assertThat(lastLocationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCountryIsRequired() throws Exception {
        int databaseSizeBeforeTest = lastLocationRepository.findAll().size();
        // set the field null
        lastLocation.setCountry(null);

        // Create the LastLocation, which fails.
        LastLocationDTO lastLocationDTO = lastLocationMapper.lastLocationToLastLocationDTO(lastLocation);

        restLastLocationMockMvc.perform(post("/api/last-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lastLocationDTO)))
            .andExpect(status().isBadRequest());

        List<LastLocation> lastLocationList = lastLocationRepository.findAll();
        assertThat(lastLocationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLastLocations() throws Exception {
        // Initialize the database
        lastLocationRepository.saveAndFlush(lastLocation);

        // Get all the lastLocationList
        restLastLocationMockMvc.perform(get("/api/last-locations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(lastLocation.getId().intValue())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.toString())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.toString())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())));
    }

    @Test
    @Transactional
    public void getLastLocation() throws Exception {
        // Initialize the database
        lastLocationRepository.saveAndFlush(lastLocation);

        // Get the lastLocation
        restLastLocationMockMvc.perform(get("/api/last-locations/{id}", lastLocation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(lastLocation.getId().intValue()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.toString()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.toString()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingLastLocation() throws Exception {
        // Get the lastLocation
        restLastLocationMockMvc.perform(get("/api/last-locations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLastLocation() throws Exception {
        // Initialize the database
        lastLocationRepository.saveAndFlush(lastLocation);
        lastLocationSearchRepository.save(lastLocation);
        int databaseSizeBeforeUpdate = lastLocationRepository.findAll().size();

        // Update the lastLocation
        LastLocation updatedLastLocation = lastLocationRepository.findOne(lastLocation.getId());
        updatedLastLocation
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .city(UPDATED_CITY)
            .country(UPDATED_COUNTRY);
        LastLocationDTO lastLocationDTO = lastLocationMapper.lastLocationToLastLocationDTO(updatedLastLocation);

        restLastLocationMockMvc.perform(put("/api/last-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lastLocationDTO)))
            .andExpect(status().isOk());

        // Validate the LastLocation in the database
        List<LastLocation> lastLocationList = lastLocationRepository.findAll();
        assertThat(lastLocationList).hasSize(databaseSizeBeforeUpdate);
        LastLocation testLastLocation = lastLocationList.get(lastLocationList.size() - 1);
        assertThat(testLastLocation.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testLastLocation.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testLastLocation.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testLastLocation.getCountry()).isEqualTo(UPDATED_COUNTRY);

        // Validate the LastLocation in Elasticsearch
        LastLocation lastLocationEs = lastLocationSearchRepository.findOne(testLastLocation.getId());
        assertThat(lastLocationEs).isEqualToComparingFieldByField(testLastLocation);
    }

    @Test
    @Transactional
    public void updateNonExistingLastLocation() throws Exception {
        int databaseSizeBeforeUpdate = lastLocationRepository.findAll().size();

        // Create the LastLocation
        LastLocationDTO lastLocationDTO = lastLocationMapper.lastLocationToLastLocationDTO(lastLocation);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restLastLocationMockMvc.perform(put("/api/last-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lastLocationDTO)))
            .andExpect(status().isCreated());

        // Validate the LastLocation in the database
        List<LastLocation> lastLocationList = lastLocationRepository.findAll();
        assertThat(lastLocationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteLastLocation() throws Exception {
        // Initialize the database
        lastLocationRepository.saveAndFlush(lastLocation);
        lastLocationSearchRepository.save(lastLocation);
        int databaseSizeBeforeDelete = lastLocationRepository.findAll().size();

        // Get the lastLocation
        restLastLocationMockMvc.perform(delete("/api/last-locations/{id}", lastLocation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean lastLocationExistsInEs = lastLocationSearchRepository.exists(lastLocation.getId());
        assertThat(lastLocationExistsInEs).isFalse();

        // Validate the database is empty
        List<LastLocation> lastLocationList = lastLocationRepository.findAll();
        assertThat(lastLocationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchLastLocation() throws Exception {
        // Initialize the database
        lastLocationRepository.saveAndFlush(lastLocation);
        lastLocationSearchRepository.save(lastLocation);

        // Search the lastLocation
        restLastLocationMockMvc.perform(get("/api/_search/last-locations?query=id:" + lastLocation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(lastLocation.getId().intValue())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.toString())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.toString())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LastLocation.class);
    }
}
