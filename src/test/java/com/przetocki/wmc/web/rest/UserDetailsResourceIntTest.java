package com.przetocki.wmc.web.rest;

import com.przetocki.wmc.WmcApp;

import com.przetocki.wmc.domain.UserDetails;
import com.przetocki.wmc.repository.UserDetailsRepository;
import com.przetocki.wmc.service.UserDetailsService;
import com.przetocki.wmc.repository.search.UserDetailsSearchRepository;
import com.przetocki.wmc.service.dto.UserDetailsDTO;
import com.przetocki.wmc.service.mapper.UserDetailsMapper;
import com.przetocki.wmc.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserDetailsResource REST controller.
 *
 * @see UserDetailsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WmcApp.class)
public class UserDetailsResourceIntTest {

    private static final String DEFAULT_NICK = "AAAAAAAAAA";
    private static final String UPDATED_NICK = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SURNAME = "AAAAAAAAAA";
    private static final String UPDATED_SURNAME = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_PARENT = false;
    private static final Boolean UPDATED_IS_PARENT = true;

    private static final Integer DEFAULT_AGE = 1;
    private static final Integer UPDATED_AGE = 2;

    private static final Boolean DEFAULT_ACCEPTED_TERMS = false;
    private static final Boolean UPDATED_ACCEPTED_TERMS = true;

    private static final String DEFAULT_LAST_KNOWN_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_LAST_KNOWN_LOCATION = "BBBBBBBBBB";

    private static final String DEFAULT_PHOTO_SERVER_URL = "AAAAAAAAAA";
    private static final String UPDATED_PHOTO_SERVER_URL = "BBBBBBBBBB";

    @Autowired
    private UserDetailsRepository userDetailsRepository;

    @Autowired
    private UserDetailsMapper userDetailsMapper;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserDetailsSearchRepository userDetailsSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUserDetailsMockMvc;

    private UserDetails userDetails;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        UserDetailsResource userDetailsResource = new UserDetailsResource(userDetailsService);
        this.restUserDetailsMockMvc = MockMvcBuilders.standaloneSetup(userDetailsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserDetails createEntity(EntityManager em) {
        UserDetails userDetails = new UserDetails()
            .nick(DEFAULT_NICK)
            .name(DEFAULT_NAME)
            .surname(DEFAULT_SURNAME)
            .phone(DEFAULT_PHONE)
            .isParent(DEFAULT_IS_PARENT)
            .age(DEFAULT_AGE)
            .acceptedTerms(DEFAULT_ACCEPTED_TERMS)
            .lastKnownLocation(DEFAULT_LAST_KNOWN_LOCATION)
            .photoServerUrl(DEFAULT_PHOTO_SERVER_URL);
        return userDetails;
    }

    @Before
    public void initTest() {
        userDetailsSearchRepository.deleteAll();
        userDetails = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserDetails() throws Exception {
        int databaseSizeBeforeCreate = userDetailsRepository.findAll().size();

        // Create the UserDetails
        UserDetailsDTO userDetailsDTO = userDetailsMapper.userDetailsToUserDetailsDTO(userDetails);
        restUserDetailsMockMvc.perform(post("/api/user-details")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userDetailsDTO)))
            .andExpect(status().isCreated());

        // Validate the UserDetails in the database
        List<UserDetails> userDetailsList = userDetailsRepository.findAll();
        assertThat(userDetailsList).hasSize(databaseSizeBeforeCreate + 1);
        UserDetails testUserDetails = userDetailsList.get(userDetailsList.size() - 1);
        assertThat(testUserDetails.getNick()).isEqualTo(DEFAULT_NICK);
        assertThat(testUserDetails.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testUserDetails.getSurname()).isEqualTo(DEFAULT_SURNAME);
        assertThat(testUserDetails.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testUserDetails.isIsParent()).isEqualTo(DEFAULT_IS_PARENT);
        assertThat(testUserDetails.getAge()).isEqualTo(DEFAULT_AGE);
        assertThat(testUserDetails.isAcceptedTerms()).isEqualTo(DEFAULT_ACCEPTED_TERMS);
        assertThat(testUserDetails.getLastKnownLocation()).isEqualTo(DEFAULT_LAST_KNOWN_LOCATION);
        assertThat(testUserDetails.getPhotoServerUrl()).isEqualTo(DEFAULT_PHOTO_SERVER_URL);

        // Validate the UserDetails in Elasticsearch
        UserDetails userDetailsEs = userDetailsSearchRepository.findOne(testUserDetails.getId());
        assertThat(userDetailsEs).isEqualToComparingFieldByField(testUserDetails);
    }

    @Test
    @Transactional
    public void createUserDetailsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userDetailsRepository.findAll().size();

        // Create the UserDetails with an existing ID
        userDetails.setId(1L);
        UserDetailsDTO userDetailsDTO = userDetailsMapper.userDetailsToUserDetailsDTO(userDetails);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserDetailsMockMvc.perform(post("/api/user-details")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userDetailsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<UserDetails> userDetailsList = userDetailsRepository.findAll();
        assertThat(userDetailsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNickIsRequired() throws Exception {
        int databaseSizeBeforeTest = userDetailsRepository.findAll().size();
        // set the field null
        userDetails.setNick(null);

        // Create the UserDetails, which fails.
        UserDetailsDTO userDetailsDTO = userDetailsMapper.userDetailsToUserDetailsDTO(userDetails);

        restUserDetailsMockMvc.perform(post("/api/user-details")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userDetailsDTO)))
            .andExpect(status().isBadRequest());

        List<UserDetails> userDetailsList = userDetailsRepository.findAll();
        assertThat(userDetailsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPhoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = userDetailsRepository.findAll().size();
        // set the field null
        userDetails.setPhone(null);

        // Create the UserDetails, which fails.
        UserDetailsDTO userDetailsDTO = userDetailsMapper.userDetailsToUserDetailsDTO(userDetails);

        restUserDetailsMockMvc.perform(post("/api/user-details")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userDetailsDTO)))
            .andExpect(status().isBadRequest());

        List<UserDetails> userDetailsList = userDetailsRepository.findAll();
        assertThat(userDetailsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIsParentIsRequired() throws Exception {
        int databaseSizeBeforeTest = userDetailsRepository.findAll().size();
        // set the field null
        userDetails.setIsParent(null);

        // Create the UserDetails, which fails.
        UserDetailsDTO userDetailsDTO = userDetailsMapper.userDetailsToUserDetailsDTO(userDetails);

        restUserDetailsMockMvc.perform(post("/api/user-details")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userDetailsDTO)))
            .andExpect(status().isBadRequest());

        List<UserDetails> userDetailsList = userDetailsRepository.findAll();
        assertThat(userDetailsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAcceptedTermsIsRequired() throws Exception {
        int databaseSizeBeforeTest = userDetailsRepository.findAll().size();
        // set the field null
        userDetails.setAcceptedTerms(null);

        // Create the UserDetails, which fails.
        UserDetailsDTO userDetailsDTO = userDetailsMapper.userDetailsToUserDetailsDTO(userDetails);

        restUserDetailsMockMvc.perform(post("/api/user-details")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userDetailsDTO)))
            .andExpect(status().isBadRequest());

        List<UserDetails> userDetailsList = userDetailsRepository.findAll();
        assertThat(userDetailsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUserDetails() throws Exception {
        // Initialize the database
        userDetailsRepository.saveAndFlush(userDetails);

        // Get all the userDetailsList
        restUserDetailsMockMvc.perform(get("/api/user-details?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].nick").value(hasItem(DEFAULT_NICK.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].surname").value(hasItem(DEFAULT_SURNAME.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].isParent").value(hasItem(DEFAULT_IS_PARENT.booleanValue())))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE)))
            .andExpect(jsonPath("$.[*].acceptedTerms").value(hasItem(DEFAULT_ACCEPTED_TERMS.booleanValue())))
            .andExpect(jsonPath("$.[*].lastKnownLocation").value(hasItem(DEFAULT_LAST_KNOWN_LOCATION.toString())))
            .andExpect(jsonPath("$.[*].photoServerUrl").value(hasItem(DEFAULT_PHOTO_SERVER_URL.toString())));
    }

    @Test
    @Transactional
    public void getUserDetails() throws Exception {
        // Initialize the database
        userDetailsRepository.saveAndFlush(userDetails);

        // Get the userDetails
        restUserDetailsMockMvc.perform(get("/api/user-details/{id}", userDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userDetails.getId().intValue()))
            .andExpect(jsonPath("$.nick").value(DEFAULT_NICK.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.surname").value(DEFAULT_SURNAME.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()))
            .andExpect(jsonPath("$.isParent").value(DEFAULT_IS_PARENT.booleanValue()))
            .andExpect(jsonPath("$.age").value(DEFAULT_AGE))
            .andExpect(jsonPath("$.acceptedTerms").value(DEFAULT_ACCEPTED_TERMS.booleanValue()))
            .andExpect(jsonPath("$.lastKnownLocation").value(DEFAULT_LAST_KNOWN_LOCATION.toString()))
            .andExpect(jsonPath("$.photoServerUrl").value(DEFAULT_PHOTO_SERVER_URL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingUserDetails() throws Exception {
        // Get the userDetails
        restUserDetailsMockMvc.perform(get("/api/user-details/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserDetails() throws Exception {
        // Initialize the database
        userDetailsRepository.saveAndFlush(userDetails);
        userDetailsSearchRepository.save(userDetails);
        int databaseSizeBeforeUpdate = userDetailsRepository.findAll().size();

        // Update the userDetails
        UserDetails updatedUserDetails = userDetailsRepository.findOne(userDetails.getId());
        updatedUserDetails
            .nick(UPDATED_NICK)
            .name(UPDATED_NAME)
            .surname(UPDATED_SURNAME)
            .phone(UPDATED_PHONE)
            .isParent(UPDATED_IS_PARENT)
            .age(UPDATED_AGE)
            .acceptedTerms(UPDATED_ACCEPTED_TERMS)
            .lastKnownLocation(UPDATED_LAST_KNOWN_LOCATION)
            .photoServerUrl(UPDATED_PHOTO_SERVER_URL);
        UserDetailsDTO userDetailsDTO = userDetailsMapper.userDetailsToUserDetailsDTO(updatedUserDetails);

        restUserDetailsMockMvc.perform(put("/api/user-details")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userDetailsDTO)))
            .andExpect(status().isOk());

        // Validate the UserDetails in the database
        List<UserDetails> userDetailsList = userDetailsRepository.findAll();
        assertThat(userDetailsList).hasSize(databaseSizeBeforeUpdate);
        UserDetails testUserDetails = userDetailsList.get(userDetailsList.size() - 1);
        assertThat(testUserDetails.getNick()).isEqualTo(UPDATED_NICK);
        assertThat(testUserDetails.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testUserDetails.getSurname()).isEqualTo(UPDATED_SURNAME);
        assertThat(testUserDetails.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testUserDetails.isIsParent()).isEqualTo(UPDATED_IS_PARENT);
        assertThat(testUserDetails.getAge()).isEqualTo(UPDATED_AGE);
        assertThat(testUserDetails.isAcceptedTerms()).isEqualTo(UPDATED_ACCEPTED_TERMS);
        assertThat(testUserDetails.getLastKnownLocation()).isEqualTo(UPDATED_LAST_KNOWN_LOCATION);
        assertThat(testUserDetails.getPhotoServerUrl()).isEqualTo(UPDATED_PHOTO_SERVER_URL);

        // Validate the UserDetails in Elasticsearch
        UserDetails userDetailsEs = userDetailsSearchRepository.findOne(testUserDetails.getId());
        assertThat(userDetailsEs).isEqualToComparingFieldByField(testUserDetails);
    }

    @Test
    @Transactional
    public void updateNonExistingUserDetails() throws Exception {
        int databaseSizeBeforeUpdate = userDetailsRepository.findAll().size();

        // Create the UserDetails
        UserDetailsDTO userDetailsDTO = userDetailsMapper.userDetailsToUserDetailsDTO(userDetails);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restUserDetailsMockMvc.perform(put("/api/user-details")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userDetailsDTO)))
            .andExpect(status().isCreated());

        // Validate the UserDetails in the database
        List<UserDetails> userDetailsList = userDetailsRepository.findAll();
        assertThat(userDetailsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteUserDetails() throws Exception {
        // Initialize the database
        userDetailsRepository.saveAndFlush(userDetails);
        userDetailsSearchRepository.save(userDetails);
        int databaseSizeBeforeDelete = userDetailsRepository.findAll().size();

        // Get the userDetails
        restUserDetailsMockMvc.perform(delete("/api/user-details/{id}", userDetails.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean userDetailsExistsInEs = userDetailsSearchRepository.exists(userDetails.getId());
        assertThat(userDetailsExistsInEs).isFalse();

        // Validate the database is empty
        List<UserDetails> userDetailsList = userDetailsRepository.findAll();
        assertThat(userDetailsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchUserDetails() throws Exception {
        // Initialize the database
        userDetailsRepository.saveAndFlush(userDetails);
        userDetailsSearchRepository.save(userDetails);

        // Search the userDetails
        restUserDetailsMockMvc.perform(get("/api/_search/user-details?query=id:" + userDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].nick").value(hasItem(DEFAULT_NICK.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].surname").value(hasItem(DEFAULT_SURNAME.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].isParent").value(hasItem(DEFAULT_IS_PARENT.booleanValue())))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE)))
            .andExpect(jsonPath("$.[*].acceptedTerms").value(hasItem(DEFAULT_ACCEPTED_TERMS.booleanValue())))
            .andExpect(jsonPath("$.[*].lastKnownLocation").value(hasItem(DEFAULT_LAST_KNOWN_LOCATION.toString())))
            .andExpect(jsonPath("$.[*].photoServerUrl").value(hasItem(DEFAULT_PHOTO_SERVER_URL.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserDetails.class);
    }
}
